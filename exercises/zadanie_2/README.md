# Zadanie domowe nr 2

## Testowanie strony miasta Gdańska

### Cel testów

Miasto Gdańsk prowadzi całkiem rozbudowany serwis informacyjny, dostępny pod adresem: www.gdansk.pl. Firma, która wygrała przetarg na przetestowanie strony, zleciła nam napisanie testów automatycznych z wykorzystaniem technologii Selenium WebDrver.

Testy mają dowieść, że kluczowe elementy informacyjne są łatwo dostępne dla osób odwiedzających stronę.

### Dokumentacja zadania

Aktualny opis zadania i instrukcje wykonania znajdują się na stronie wiki: [Zadanie 2 - opis](https://git.codeme.pl/testowanie/selenium-e1/Zadania/zadanie_2/wikis/home)

