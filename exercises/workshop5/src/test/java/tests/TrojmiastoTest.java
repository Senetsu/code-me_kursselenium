package tests;

import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;

import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class TrojmiastoTest {

    private WebDriver driver;

    @Before
    public void setUp() throws InterruptedException {
        driver = WebDriverFactory.getDriver();
        driver.get("https://www.trojmiasto.pl");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testHeader() {
    //List<WebElement> elements = driver.findElements(By.xpath("//*[@class=\"col-rt\"]/a[1]"));
    String headerMainPage = driver.findElement(By.xpath("//*[@class=\"col-rt\"]/a[1]/h2")).getText();
    driver.findElement(By.xpath("//*[@class=\"col-rt\"]/a[1]/@href")).click();
    driver.manage().window().fullscreen();
    String headerSubPage = driver.findElement(By.xpath("//*[@class=\"text-container\"]/h1")).getText();

    //Assert.assertTrue(elements.size() >= 1);
    Assert.assertTrue(headerMainPage.equals(headerSubPage));

    }
}