# Pojedynek z Syberią

*"Powiem ci, co w życiu jest najważniejsze. Nie szukaj tego, co złudne: majątku, sławy – żeby to zdobyć, człowiek rujnuje nerwy przez dziesięciolecia, a może stracić to w ciągu jednej doby. Żyj z poczuciem spokojnej wyższości nad życiem, nie bój się biedy, nie żałuj straconego szczęścia, przecież nigdy nie jest tak, że ani goryczy do dna, ani słodyczy do pełna…Ciesz się, że nie marzniesz, że głód i pragnienie nie rwą ci wnętrzności. Jeśli nie masz przetrąconego kręgosłupa, jeśli obie nogi chodzą, obie ręce się ruszają – to komu tu zazdrościć? (…)Przetrzyj oczy, a najwyżej ceń sobie tych, którzy cię kochają i którzy ci dobrze życzą."*

[Romuald Koperski](https://pl.wikipedia.org/wiki/Romuald_Koperski)


## Opis zadania

Popularną (niegdyś...) kategorią stron internetowych były różnego rodzaju blogi. Obecnie ta funkcja została w zasadzie zdominowana przez Facebooka, ale wciąż powstają różne tego rodzaju strony - jak wspomniane blogi czy systemy CMS (Content Management System). Przy testowaniu tego rodzaju projektów nie powinniśmy opierać się na treści, gdyż może być ona zmienna. Niemniej czasami chcemy przetestować, czy danego rodzaju artykuł został prawidłowo otagowany i skategoryzowany. Nic również nie stoi na przeszkodzie, żeby sprawdzić obecność stałych elementów strony - właściwych sekcji, forum, itp.

## Przypadki testowe do zaimplementowania

### 1. Wyszukanie określonej treści

1. Wejdź na stronę http://www.wrobels.pl i wyszukaj hasło **Pojedynek z Syberią**
2. Sprawdź, czy lista wyników zawiera link do artykułu **Pojedynek z Syberia – historia pewnej ekspedycji**
3. Przejdź do tego artykułu
4. Sprawdź, czy tytuł znalezionego artykułu jest właściwy

### 2. Sprawdzenie podstawowych elementów strony

1. Wejdź na stronę http://www.wrobels.pl/pojedynek-z-syberia/
2. Sprawdź, czy załadowana strona zawiera następujące sekcje:
  a) Najnowsze wpisy
  b) Najnowsze komentarze
  c) O autorze


### 3. Sprawdzenie prawidłowości hashtagów

1. Wejdź na stronę http://www.wrobels.pl/pojedynek-z-syberia/
2. Sprawdź, czy pod artykułem znajdują się wszystkie hashtagi: #Inspiracje podróżnicze, #Książka, #Ludzie, #Niesamowite miejsca


###### Dla chętnych: zrobić sprawdzenie sekcji i hashtagów nie na zasadzie pojedynczych asercji, tylko za pomocą listy. Wtedy w pętlach możemy sprawdzić asercję na poszczególnych sekcji i hashtagów


## Wskazówki do uruchomienia

Analogicznie do zadania_2: [Przygotowanie środowiska](https://git.codeme.pl/testowanie/selenium-e1/Zadania/zadanie_2/wikis/Przygotowanie%20%C5%9Brodowiska)
