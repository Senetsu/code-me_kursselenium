package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {
    public static WebDriver getDriver() {
        String driverBinaryPath;
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            driverBinaryPath = "webdriver\\win\\chromedriver.exe";
        } else if (os.contains("nux") || os.contains("nix")) {
            driverBinaryPath = "webdriver/linux/chromedriver";
        } else if (os.contains("mac")) {
            driverBinaryPath = "webdriver/mac/chromedriver";
        } else {
            driverBinaryPath = "";
        }

        System.setProperty("webdriver.chrome.driver", driverBinaryPath);

        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1000x800");
        WebDriver driver = new ChromeDriver(options);
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver;
    }

}
