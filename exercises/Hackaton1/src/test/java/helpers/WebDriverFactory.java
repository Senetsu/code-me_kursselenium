package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    public static WebDriver getDriver() {
        if(System.getProperty("webdriver") != null && System.getProperty("webdriver").contains("firefox")) {
            return getFirefoxDriver();
        }
        return getChromeDriver();
    }

    public static WebDriver getChromeDriver() {
        String driverBinaryPath = WebDriverFactory.getSystemPath()
                + (isWindows() ? "chromedriver.exe"
                : "chromedriver");
        System.setProperty("webdriver.chrome.driver", driverBinaryPath);

        ChromeOptions options = new ChromeOptions();
        //options.setHeadless(true);
        options.addArguments("--window-size=1920,1080");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver;
    }

    public static WebDriver getFirefoxDriver() {
        String driverBinaryPath = WebDriverFactory.getSystemPath()
                + (isWindows() ? "geckodriver.exe"
                : "geckodriver");
        System.setProperty("webdriver.gecko.driver", driverBinaryPath);
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("-headless");
        options.addArguments("--width=1800");
        options.addArguments("--height=900");
        WebDriver driver = new FirefoxDriver(options);
        return driver;
    }

    public static String getSystemPath() {
        String driverBinaryPath;
        if( isWindows() ) {
            driverBinaryPath = "webdriver\\win\\";
        } else if ( isLinux() ) {
            driverBinaryPath = "webdriver/linux/";
        } else if ( isMac() ) {
            driverBinaryPath = "webdriver/mac/";
        } else {
            driverBinaryPath = "";
        }
        return driverBinaryPath;
    }

    private static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("win");
    }

    private static boolean isMac() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }

    private static boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().contains("nux") ||
               System.getProperty("os.name").toLowerCase().contains("nix");
    }

}
