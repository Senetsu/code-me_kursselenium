package pages;

import helpers.ProjectData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class LoginPage {
    private WebDriver driver;

    private By username = By.id("username");
    private By submit = By.id("submit");

    // konstruktor:
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private By checkLogin = By.xpath("//*[contains(text(), \"Logout\")]");

    public void login(String userLogin) {
        driver.get(ProjectData.getBaseUrl() + "/login");
        driver.findElement(username).clear();
        driver.findElement(username).sendKeys(userLogin);
        driver.findElement(submit).click();
    }

    public void checkCorrectLogin() {
        driver.findElement(checkLogin).getSize();;
    }


}