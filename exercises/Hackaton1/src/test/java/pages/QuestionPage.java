package pages;

import helpers.ProjectData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class QuestionPage {
    private WebDriver driver;

    private By checkbox = By.cssSelector("body");
    private By answer = By.name("answer");
    private By submit = By.id("submit");
    private By checkLogin = By.xpath("//*[contains(text(), \"Logout\")]");

    // konstruktor:
    public QuestionPage(WebDriver driver) {
        this.driver = driver;
    }

    public void answerQuestion() {
        driver.findElement(checkbox).click();
        driver.findElement(answer).click();
        driver.findElement(submit).click();
    }

    public void checkCorrectLogin() {
        driver.findElement(checkLogin).getSize();;
    }



}
