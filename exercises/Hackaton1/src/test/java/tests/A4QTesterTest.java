package tests;

import helpers.ProjectData;
import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.*;


public class A4QTesterTest {

    private WebDriver driver;

    @Before
    public void setUp() {
        driver = WebDriverFactory.getDriver();
        driver.get(ProjectData.getBaseUrl());
    }
    @After
    public void tearDown() {
        driver.quit();
    }

}
