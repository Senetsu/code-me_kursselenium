package tests;

import helpers.ProjectData;
import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.LoginPage;

import java.util.List;

public class LoginTest {

    private WebDriver driver;
    private LoginPage loginPage;

    @Before
    public void setUp() {
        driver = WebDriverFactory.getDriver();
        driver.get(ProjectData.getBaseUrl());
        loginPage = new LoginPage(driver);
    }
    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testLogin1(){
        loginPage.login("user001");
        List<WebElement> elements = driver.findElements(By.xpath("//*[contains(text(), \"Logout\")]"));
        assert(elements.size() > 0);
    }

}
