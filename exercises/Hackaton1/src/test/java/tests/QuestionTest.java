package tests;

import helpers.ProjectData;
import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.LoginPage;
import pages.QuestionPage;

import java.util.List;

public class QuestionTest {

    private WebDriver driver;
    private LoginPage loginPage;
    private QuestionPage questionPage;

    @Before
    public void setUp() {
        driver = WebDriverFactory.getDriver();
        driver.get(ProjectData.getBaseUrl());
        loginPage = new LoginPage(driver);
    }
    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void sendAnswer(){
        loginPage.login("user001");
        questionPage.answerQuestion();


        {
            List<WebElement> elements = driver.findElements(By.xpath("//*[contains(text(), \'Question No\')]"));
            assert(elements.size() > 0);
        }
        {
            List<WebElement> elements = driver.findElements(By.xpath("//ol"));
            assert(elements.size() > 0);
        }
    }

}
