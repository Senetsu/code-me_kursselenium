package tests;

import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.InputPage;

public class InputTest {
    private WebDriver driver;
    private InputPage inputPage;

    @Before
    public void setUp(){
        driver = WebDriverFactory.getDriver();
        inputPage = new InputPage(driver);
        inputPage.goToPage();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testInput1() {
        inputPage.setInput("2");
        Assert.assertEquals("2", inputPage.getOutput());
    }
    @Test
    public void testInput2() {
        inputPage.setInput("-20000");
        Assert.assertNotEquals("20000", inputPage.getOutput());
    }
    @Test
    public void testInput3() {
        inputPage.setInput("0");
        Assert.assertEquals("0", inputPage.getOutput());
    }
}
