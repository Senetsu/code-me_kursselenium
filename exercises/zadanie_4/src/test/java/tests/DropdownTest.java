package tests;

import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.DropdownPage;

public class DropdownTest {
    private WebDriver driver;
    private DropdownPage dropdownPage;

    @Before
    public void setUp() {
        driver = WebDriverFactory.getDriver();
        dropdownPage = new DropdownPage(driver);
        dropdownPage.goToPage();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testSelectedDropdown1() {
        dropdownPage.setSelectionDropdown1(true);
        Assert.assertTrue(dropdownPage.getSelectedDropdown1());
    }

    @Test
    public void testSelectedDropdown2() {
        dropdownPage.setSelectionDropdown2(true);
        Assert.assertTrue(dropdownPage.getSelectedDropdown2());
    }
}
