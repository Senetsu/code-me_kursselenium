package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InputPage extends HerokuMainPage {
    // konstruktor:
    public InputPage(WebDriver driver) {
        super(driver);
    }

    private By input = By.cssSelector("input");
    private By output = By.cssSelector("#content > div > div > div > input");

    public void goToPage() {
        driver.get(BASE_URL + "/inputs");
    }

    public void setInput(String value) {
        driver.findElement(input).sendKeys(value);
    }

    public String getOutput() {
        String getOut = driver.findElement(output).getAttribute("value");
        return getOut;
    }
}
