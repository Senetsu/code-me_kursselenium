package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DropdownPage extends HerokuMainPage {

    private By dropdown1 = By.cssSelector("#dropdown > option:nth-child(2)");
    private By dropdown2 = By.cssSelector("#dropdown > option:nth-child(3)");
    private By clickDropdown = By.id("dropdown");


    public DropdownPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        driver.get(BASE_URL + "/dropdown");
    }

    public void setSelectionDropdown1(boolean selected) {
        driver.findElement(clickDropdown).click();
        WebElement dropdown =  driver.findElement(dropdown1);
        setSelected(dropdown, selected);
    }

    public void setSelectionDropdown2(boolean selected) {
        driver.findElement(clickDropdown).click();
        WebElement dropdown =  driver.findElement(dropdown2);
        setSelected(dropdown, selected);
    }

    public boolean getSelectedDropdown1() {
        return driver.findElement(dropdown1).isSelected();
    }

    public boolean getSelectedDropdown2() {
        return driver.findElement(dropdown2).isSelected();
    }

    private void setSelected(WebElement element, boolean selected) {
        if(element.isSelected() != selected) {
            element.click();
        }
    }

}