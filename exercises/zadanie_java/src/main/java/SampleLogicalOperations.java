public class SampleLogicalOperations {

    /**
     * Should I commute by bike
     * Napisz metodę, która rozstrzyga za nas, czy mamy jechać rowerem, czy jednak wybierzemy samochód. Kryteria wyboru
     * definiuje tabela decyzji:
     *
     *  decyzja |  dystans (distance) | nastrój (mood) | opady (rain)
     *  --------|---------------------|----------------|-------------
     *   TAK    |    do 50km          |     dobry      |    nie
     *   TAK    |    do 20km          |     słaby      |    nie
     *   TAK    |    do 20km          |     dobry      |    tak
     *   TAK    |    do 10km          |     słaby      |    tak
     *   NIE    |              w pozostałych przypadkach
     *
     * @param distance liczba całkowita - kilometry do przejechania
     * @param mood true - dobry nastrój, false - słaby nastrój
     * @param rain true - pada, false - nie pada
     * @return true, jeżeli jedziemy rowerem, false w przeciwnym wypadku
     */
    public boolean shouldICommuteByBike(int distance, boolean mood, boolean rain) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        if (distance < 50 && mood == true && rain == false)
            return true;
        else if (distance < 20 && mood == false && rain == false)
            return true;
        else if (distance < 20 && mood == true && rain == true)
            return true;
        else if (distance < 10 && mood == false && rain == true)
            return true;
        else
            return false;
    }


    /**
     * Tworzymy funkcję obliczającą cenę pokoju hotelowego. Cena bazowa jest modyfikowana w zależności od następującej
     * tabeli decyzji:
     *
     * Współczynnik | Stały  | Targi | Promocja
     * modyfikacji  | klient |       |
     * ceny         |        |       |
     * -------------|--------|-------|----------
     * 100%         |  nie   | nie   | nie
     * 150%         |        | tak   |
     * 80%          |  tak   | nie   |
     * 80%          |        | nie   | tak
     *
     * @param basePrice - cena bazowa
     * @param loyalCustomer - true - wierny klient, false - normalny klient
     * @param fair true - odbywają się targi, false - nie ma targów
     * @param discount true - aktywna promocja, false - nieaktywna promocja
     * @return cena doby hotelowej pomnożona przez współczynnik modyfikacji
     */
    public double getHotelPrice(double basePrice, boolean loyalCustomer, boolean fair, boolean discount) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        if (loyalCustomer == false && fair == false && discount == false)
            return basePrice * 1;
        else if (fair == true)
            return basePrice * 1.5;
        else if (loyalCustomer == true && fair == false)
            return basePrice * 0.8;
        else if (fair == false && discount == true)
            return basePrice * 0.8;
        else
            return basePrice;
    }

    /**
     * Określamy, czy klient może kupić bilet ulgowy. Bilet ulgowy przysługuje:
     * 1. Młodzieży do 18 roku życia
     * 2. Osobom do 26 roku życia, pod warunkiem, że wciąż się uczą
     * 3. Seniorom powyżej 65 roku życia
     *
     * @param age
     * @param student
     * @return true, jeżeli można sprzedać ulgowy bilet, false w przeciwnym wypadku
     */
    public boolean canGiveDiscount(int age, boolean student) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        if (age <= 18)
            return true;
        else if (age <= 26 && student == true)
            return true;
        else if (age > 65)
            return true;
        else
            return false;
    }


}
