import jdk.nashorn.internal.ir.SplitNode;

public class SampleTextOperations {

    /**
     * Na potrzeby aplikacji generującej korespondencję seryjną tworzymy metodę, która z podanego imienia i nazwiska
     * utworzy odpowiednią formę grzecznościową, rozpoczynającą się od Pani lub Pan. Dodatkowo, aby ułatwić szybkie
     * odczytywanie nazwiska, nazwisko powinno być napisane wielkimi literami, np.
     * Pani Ewelina NOWAK
     * Pan Antoni Kowalski
     * <p>
     * Płeć podanego adresata można rozpoznać na podstawie zaleceń Rady języka polskiego:
     * http://www.rjp.pan.pl/index.php?option=com_content&view=article&id=611&Itemid=142
     * - w uproszczeniu, jeżeli imię kończy się na 'a', to jest żeńskie, w przeciwnym wypadku męskie
     */
    public String fullNameWithTitle(String firstName, String lastName) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");

        String newFirstName = firstName.substring(0, 1).toUpperCase() + firstName.toLowerCase().substring(1);
        String name;
        if (firstName.matches("(.*)a"))
            name = "Pani " + newFirstName + " " + lastName.toUpperCase();
        else
            name = "Pan " + newFirstName + " " + lastName.toUpperCase();
        return name;
    }


    /**
     * Piszemy metodę, która dostaje tekst logu programu podzielony na linie i liczy, w ilu liniach wystąpił wskazany
     * tekst.
     * - metoda powinna sprawdzać tekst niezależnie, czy wystąpił wielkimi literami, czy małymi
     * - jeżeli szukany tekst wystąpi więcej niż jeden raz w danej linii, liczymy go tylko jeden raz.
     * <p>
     * Np dla tekstu.
     * Ala ma kota,
     * A kot ma Alę.
     * Ala często karmi swojego kota. Ala ma również psa.
     * ALA TO DZIEWCZYNKA Z ELEMENTARZA.
     * <p>
     * Jeżeli szukamy frazy "ala", powinniśmy otrzymać w wyniku liczbę 3 (Ala wystąpiła w 3 liniach logu)
     *
     * @param text   tekst, w którym szukamy frazy
     * @param phrase szukana fraza
     * @return ilość linii, w których znaleziono szukaną frazę
     */
    public int countLinesWithPhraseOccurence(String[] text, String phrase) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        int number = 0;

        for (int i = 0; i < text.length; i++) {
            if (text[i].toLowerCase().contains(phrase))
                number += 1;
        }
        return number;
    }
}




