public class SampleCalculations {

    /**
     * Metoda zwraca kwadrat podanej liczby
     * @param a
     * @return kwadrat liczby
     */
    public int calculateSquare(int a) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        return a*a;
    }

    /**
     * Metoda wyszukuje największą wartość w podanej tablicy elementów
     * @param arrayOfInt wejściowa tablica
     * @return największa wartość w tablicy
     */
    public int findMaxValue(int[] arrayOfInt)
    {
        // dla pustej tablicy zwracamy Integer.MIN_VALUE
        int max = Integer.MIN_VALUE;

        //TODO: usuń te 2 linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");

        for (int i = 0; i < arrayOfInt.length; i++) {
            if (arrayOfInt[i] > max) {
                max = arrayOfInt[i];
            }
        }
        return max;
    }

    /**
     * Metoda wyszukuje najmniejszą wartość w podanej tablicy elementów
     * @param arrayOfInt wejściowa tablica
     * @return najmniejszą wartość w tablicy
     */

    public int findMinValue(int[] arrayOfInt)
    {

        int min = Integer.MAX_VALUE;

        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        for (int i = 0; i < arrayOfInt.length; i++) {
            if (arrayOfInt[i] < min) {
                min = arrayOfInt[i];
            }
        }
        return min;
    }

    /**
     * Metoda licząca sumę wszystkich podanych argumentów.
     * Za pomocą takiej składni: double... values mówimy kompilatorowi, że do tej metody może być podana różna ilość
     * parametrów tego samego typu - w tym wypadku double. Może to być 0, 1, 15, ... n argumentów
     * Poruszamy się po tych wartościach analogcznie do tablicy, pamiętając, że pierwszy element ma indeks 0. I tak:
     * values[0] - pierwsza z podanych wartości
     * values[1] - druga z podanych wartości, itd.
     *
     * Wskazówka: zastosuj pętlę for
     *
     * @param values wartości do obliczenia sumy
     * @return suma wszystkich podanych wartości
     */
    public double calculateSumOfVariableNumberOfParameters(double... values) {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        double sum = 0;
        for (int i = 0; i < values.length; i++)
            {
                sum += values[i];
            }
        return sum;
        }


    /**
     * Metoda oblicza średnią arytmetyczną z elementów tablicy.
     * Wskazówka: możesz wykorzystać metodę calculateSumOfVariableNumberOfParameters do obliczenia sumy elementów
     * @param arrayOfNumbers tablica z wartościami, których średnią chcemy policzyć
     * @return średnia arytmetyczna elementów tablicy
     */
    public double calculateMeanOfArray(double[] arrayOfNumbers) // ma zwracać 0 jeżeli arrayOfNumbers.length == 0
    {
        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");
        double array = 0;
        array = calculateSumOfVariableNumberOfParameters(arrayOfNumbers) / arrayOfNumbers.length;
        return array;

    }

    /**
     * Napisz metodę, która wypełnia tablicę w ten sposób, że 2 pierwsze elementy to 0, 1, a każdy kolejny element jest sumą
     * dwóch poprzednich. Czyli np.: 0, 1, 1, 2, 3, 5, 8, 13, ...
     *
     * @param tableLength ilość elementów tablicy do wypełnienia
     * @return suma elementów tablicy
     * // parę wskazówek:
     * // 1. tablica o odpowiedniej długości już jest utworzona
     * // 2. utworzyłem zmienną, do której będziesz dodawać elementy
     * // 3. możesz zastosować pętlę for, która startuje od 3 elementu tablicy, czyli int i = 2; < tableLength
     * // 4. sumę elementów tablicy należy policzyć w pętli for. Możesz napisać 2 pętle - jedna wypełniająca tablicę \
     * // wartościami, druga licząca sumę (i może od tego zacznij). Zastanów się, czy można to zrobić w jednej pętli.
     */
    public int sumOfFibonacciElements(int tableLength) //0, 1, 1, 2, 3, 5, 8, 13, ...
    {
        if (tableLength <= 1) return 0;  // zabezpieczenie przed za małą długością tablicy
        int[] fibonacci = new int[tableLength];
        int sum = 1;
        fibonacci[0] = 0;
        fibonacci[1] = 1;
        for (int i = 2; i < tableLength; i++)
        {
            fibonacci[i] = fibonacci[i - 1 ]+ fibonacci[i - 2];
            sum += fibonacci[i];
        }
        //wskazówka: zastosuj pętlę for, która rozpoczyna się od 3 elementu (czyli index=2)

        //TODO: usuń te linie i zaimplementuj metodę
        //throw new RuntimeException("Not implemented yet.");

        return sum;
    }


}
