import org.junit.Assert;
import org.junit.Test;


public class SampleCalculationsTest {

    private SampleCalculations calc = new SampleCalculations();

    @Test
    public void testIntegerSquare() {
        Assert.assertEquals(4, calc.calculateSquare(2));
    }

    @Test
    public void testNegativeIntegerSquare() {
        Assert.assertEquals(16, calc.calculateSquare(-4));
    }

    @Test
    public void findMaxValue() {
        Assert.assertEquals(20, calc.findMaxValue(new int[]{1, 3, -4, 20, 17}));
    }

    @Test
    public void findMinValue() {
        Assert.assertEquals(-4, calc.findMinValue(new int[]{1, 3, -4, 20, 17}));
    }

    @Test
    public void calculateSumOfVariableNumberOfParameters() {
        Assert.assertEquals(17, calc.calculateSumOfVariableNumberOfParameters(2, -4, 8, 6.5, 4.5), 0.001);
    }

    @Test
    public void calculateMeanOfArray() {
        Assert.assertEquals(3.4, calc.calculateMeanOfArray(new double[] {2, -4, 8, 6.5, 4.5}), 0.001);
    }

    @Test
    public void generateFibonacci() {
        Assert.assertEquals(54, calc.sumOfFibonacciElements(9));
    }
}
