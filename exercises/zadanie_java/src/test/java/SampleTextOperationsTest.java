import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SampleTextOperationsTest {

    private SampleTextOperations sampleTextOperations = new SampleTextOperations();

    @Test
    public void fullNameWithTitleMr() {
        Assert.assertEquals("Pan Jan KOWALSKI", sampleTextOperations.fullNameWithTitle("Jan", "Kowalski"));
    }

    @Test
    public void fullNameWithTitleMrs() {
        Assert.assertEquals("Pani Janina KOWALSKA", sampleTextOperations.fullNameWithTitle("Janina", "Kowalska"));
    }

    @Test
    public void countPhraseOccurence() {

        String[] text = new String[] {
                "Ala ma kota",
                "A kot ma Alę.",
                "Ala często karmi swojego kota. Ala ma również psa.",
                "ALA TO DZIEWCZYNKA Z ELEMENTARZA."
        };
        Assert.assertEquals(3, sampleTextOperations.countLinesWithPhraseOccurence(text, "ala"));

    }

    @Test
    public void countPhraseOccurenceNoPhraseFound() {

        String[] text = new String[] {
                "Ala ma kota",
                "A kot ma Alę.",
                "Ala często karmi swojego kota. Ala ma również psa.",
                "ALA TO DZIEWCZYNKA Z ELEMENTARZA."
        };
        Assert.assertEquals(0, sampleTextOperations.countLinesWithPhraseOccurence(text, "ola"));

    }

}