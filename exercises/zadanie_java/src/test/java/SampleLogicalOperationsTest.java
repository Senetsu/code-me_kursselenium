import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SampleLogicalOperationsTest {

    private SampleLogicalOperations sampleLogicalOperations = new SampleLogicalOperations();

    /*
     *  decyzja |  dystans (distance) | nastrój (mood) | opady (rain)
     *   TAK    |    do 50km          |     dobry      |    nie
     *   TAK    |    do 20km          |     słaby      |    nie
     *   TAK    |    do 20km          |     dobry      |    tak
     *   TAK    |    do 10km          |     słaby      |    tak
     *   NIE    |              w pozostałych przypadkach
    */

    @Test
    public void shouldICommuteByBikeLongDistanceGoodMoodNoRain() {
        Assert.assertTrue(sampleLogicalOperations.shouldICommuteByBike(35, true, false));
    }

    @Test
    public void shouldICommuteByBikeMidDistanceBadMoodNoRain() {
        Assert.assertTrue(sampleLogicalOperations.shouldICommuteByBike(18, false, false));
    }

    @Test
    public void shouldICommuteByBikeMidDistanceGoodMoodRain() {
        Assert.assertTrue(sampleLogicalOperations.shouldICommuteByBike(16, true, true));
    }

    @Test
    public void shouldICommuteByBikeShortDistanceBadMoodRain() {
        Assert.assertTrue(sampleLogicalOperations.shouldICommuteByBike(9, false, true));
    }

    @Test
    public void shouldICommuteByBikeTooLongDistance() {
        Assert.assertFalse(sampleLogicalOperations.shouldICommuteByBike(199, true, false));
    }

    @Test
    public void shouldICommuteByBikeLongDistanceBadMoodNoRain() {
        Assert.assertFalse(sampleLogicalOperations.shouldICommuteByBike(39, false, false));
    }

    /*
     * Współczynnik | Stały  | Targi | Promocja
     * modyfikacji  | klient |       |
     * ceny         |        |       |
     * -------------|--------|-------|----------
     * 100%         |  nie   | nie   | nie
     * 150%         |        | tak   |
     * 80%          |  tak   | nie   |
     * 80%          |        | nie   | tak
     */

    @Test
    public void getHotelPriceRegular() {
        Assert.assertEquals(100, sampleLogicalOperations.getHotelPrice(100, false, false, false), 0.001);
    }

    @Test
    public void getHotelPriceNoFairLoyalCustomer() {
        Assert.assertEquals(80, sampleLogicalOperations.getHotelPrice(100, true, false, false), 0.001);
    }

    @Test
    public void getHotelPriceNoFairDiscount() {
        Assert.assertEquals(80, sampleLogicalOperations.getHotelPrice(100, false, false, true), 0.001);
    }

    @Test
    public void getHotelPriceNoFairLoyalDiscount() {
        Assert.assertEquals(80, sampleLogicalOperations.getHotelPrice(100, true, false, true), 0.001);
    }

    @Test
    public void getHotelPriceFair() {
        Assert.assertEquals(150, sampleLogicalOperations.getHotelPrice(100, false, true, false), 0.001);
    }

    @Test
    public void getHotelPriceFairLoyal() {
        Assert.assertEquals(150, sampleLogicalOperations.getHotelPrice(100, true, true, false), 0.001);
    }

    @Test
    public void getHotelPriceFairDiscount() {
        Assert.assertEquals(150, sampleLogicalOperations.getHotelPrice(100, false, true, true), 0.001);
    }

    @Test
    public void getHotelPriceFairLoyalDiscount() {
        Assert.assertEquals(150, sampleLogicalOperations.getHotelPrice(100, true, true, true), 0.001);
    }

    @Test
    public void canGiveDiscountAdult22NotStudent() {
        Assert.assertFalse( sampleLogicalOperations.canGiveDiscount(22, false ));
    }

    @Test
    public void canGiveDiscountAdult24Student() {
        Assert.assertTrue( sampleLogicalOperations.canGiveDiscount(24, true ));
    }

    @Test
    public void canGiveDiscountAdult33NotStudent() {
        Assert.assertFalse( sampleLogicalOperations.canGiveDiscount(33, false ));
    }

    @Test
    public void canGiveDiscountAdult33Student() {
        Assert.assertFalse( sampleLogicalOperations.canGiveDiscount(33, true ));
    }

    @Test
    public void canGiveDiscountYoungStudent() {
        Assert.assertTrue( sampleLogicalOperations.canGiveDiscount(16, true ));
    }

    @Test
    public void canGiveDiscountYoungNotStudent() {
        Assert.assertTrue( sampleLogicalOperations.canGiveDiscount(16, false ));
    }

    @Test
    public void canGiveDiscountSeniorNotStudent() {
        Assert.assertTrue( sampleLogicalOperations.canGiveDiscount(67, false ));
    }

    @Test
    public void canGiveDiscountSeniorStudent() {
        Assert.assertTrue( sampleLogicalOperations.canGiveDiscount(67, true ));
    }
}