package pages.loremipsum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoremPage2 {
    private WebDriver driver;

    public LoremPage2(WebDriver driver) {
        this.driver = driver;
        // nie dodajemy adresu stronki, bo działamy na już otwartej stronie
    }

    public String getGeneratedLorem() {
        return driver.findElement(By.xpath("//*[@id=\"generated\"]")).getText();
    }
}
