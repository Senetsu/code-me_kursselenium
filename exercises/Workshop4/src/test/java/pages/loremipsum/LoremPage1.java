package pages.loremipsum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoremPage1 {
    private WebDriver driver;

    // tworzymy konstruktor, który definiuje nam driver
    // jeżeli nie ma this. to nie odnosi sie do klasy ale do argumentu
    public LoremPage1(WebDriver driver) { this.driver = driver; }

    public void goToWebPage() { driver.get("https://pl.lipsum.com"); }

    public void fillGenerate(String actions, String number){
        driver.findElement(By.xpath("//*[@id='" + actions + "']")).click();
        WebElement amount = driver.findElement(By.xpath("//*[@id=\"amount\"]"));
        amount.clear();
        amount.sendKeys(number);

        driver.findElement(By.xpath("//*[@id=\"generate\"]")).click();
    }
}
