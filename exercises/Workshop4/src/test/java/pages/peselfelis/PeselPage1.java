package pages.peselfelis;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PeselPage1 {
    private WebDriver driver;

    // tworzymy konstruktor, który definiuje nam driver
    // jeżeli nie ma this. to nie odnosi sie do klasy ale do argumentu
    public PeselPage1(WebDriver driver) {
        this.driver = driver;
    }

    public void goToWebPage() {
        driver.get("http://pesel.felis-net.com/");
    }

    public void fillForm(String year, String month, String day, String sex) {
        driver.findElement(By.name("rok")).sendKeys(year);

        WebElement dropdown1 = driver.findElement(By.name("miesiac"));
        dropdown1.findElement(By.xpath("//option[. = '" + month + "']")).click();

        driver.findElement(By.name("dzien")).sendKeys(day);

        WebElement dropdown2 = driver.findElement(By.name("plec"));
        dropdown2.findElement(By.xpath("//option[. = '" + sex + "']")).click();

        // click submit button
        driver.findElement(By.cssSelector("input:nth-child(14)")).click();
    }
}
