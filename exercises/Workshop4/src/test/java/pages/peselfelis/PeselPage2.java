package pages.peselfelis;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PeselPage2 {
    private WebDriver driver;

    public PeselPage2(WebDriver driver) {
        this.driver = driver;
        // nie dodajemy adresu stronki, bo działamy na już otwartej stronie
    }

    public String getGeneratedPesel() {
        return driver.findElement(By.xpath("/html/body/center/center/b")).getText();
    }
}
