package tests;

import helpers.WebDriverFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.loremipsum.LoremPage1;
import pages.loremipsum.LoremPage2;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class LoremipsumTest {

    private WebDriver driver;

    LoremPage1 lorem1;
    LoremPage2 lorem2;

    @Before
    public void setUp() throws InterruptedException {
        driver = WebDriverFactory.getDriver();  //otwieramy przeglądarkę
        driver.get("https://pl.lipsum.com");

        lorem1 = new LoremPage1(driver);   // tworzymy narzędzia do otworzenia 1. strony
        lorem2 = new LoremPage2(driver);   // tworzymy narzędzia do otworzenia 2. strony
        lorem1.goToWebPage();

        // czekamy na załadowanie ciasteczek
        driver.switchTo().activeElement();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[contains(text(), 'I Agree')]")).click();
        Thread.sleep(2000);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void generateWord() {
        lorem1.fillGenerate("words", "500");

//        WebElement cbWords = driver.findElement(By.id("words"));
//
//        WebDriverWait wait = new WebDriverWait(driver, 5);
//        wait.until(ExpectedConditions.elementToBeClickable(cbWords));

        List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"lipsum\"]"));
        Assert.assertTrue(elements.size() == 1 );

        assertThat(lorem2.getGeneratedLorem(), containsString("500 słów"));

        List<WebElement> listOfParagraphs  = driver.findElement(By.xpath("//*[@id=\"lipsum\"]")).findElements(By.xpath("//p"));
        String loremIpsumText = "";

        for(WebElement paragraph : listOfParagraphs) {
            if (loremIpsumText.length() > 0) loremIpsumText += " ";
                loremIpsumText += paragraph.getText();
            }
        Assert.assertEquals(500, loremIpsumText.split("\\s+").length);
    }

    @Test
    public void generateParagraph() {
        lorem1.fillGenerate("paras", "5");

        List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"lipsum\"]"));
        Assert.assertTrue(elements.size() == 1 );

        assertThat(lorem2.getGeneratedLorem(), containsString("5 akapitów"));

        List<WebElement> listOfParagraphs  = driver.findElement(By.xpath("//*[@id=\"lipsum\"]")).findElements(By.xpath("//p"));
        Assert.assertEquals(5, listOfParagraphs.size());
    }

    @Test
    public void generateByte() {
        lorem1.fillGenerate("bytes", "200");

        List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"lipsum\"]"));
        Assert.assertTrue(elements.size() == 1 );

        assertThat(lorem2.getGeneratedLorem(), containsString("200 bajtów"));

        List<WebElement> listOfParagraphs  = driver.findElement(By.xpath("//*[@id=\"lipsum\"]")).findElements(By.xpath("//p"));
        String loremIpsumText = "";

        for(WebElement paragraph : listOfParagraphs) {
            loremIpsumText += paragraph.getText();
        }
        int countBytes= loremIpsumText.getBytes().length;

        Assert.assertEquals(200, countBytes);

    }

    @Test
    public void generateLists() {
        lorem1.fillGenerate("lists", "10");

        assertThat(lorem2.getGeneratedLorem(), containsString("10 akapitów"));

        List<WebElement> listOfParagraphs  = driver.findElement(By.xpath("//*[@id=\"lipsum\"]")).findElements(By.xpath("//ul"));
        Assert.assertEquals(10, listOfParagraphs.size());
    }

}
